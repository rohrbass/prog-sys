#include "computer.h"
#include "utils.h"

void computer_print( Computer* c ) {
	// printf("+--COMPUTER-STATE--+\n");
	// printf("+--CPU---------------------------+\n");
	// printf("| \x1B[33mPC\x1B[0m | \x1B[33mC\x1B[0m | \x1B[33mN\x1B[0m | \x1B[33mR0\x1B[0m | \x1B[33mR1\x1B[0m | \x1B[33mR2\x1B[0m | \x1B[33mR3\x1B[0m |\n");
	// printf("| %02x | %i | %i | %02x | %02x | %02x | %02x |\n", c->cpu.pc, c->cpu.C != 0, c->cpu.N != 0, c->cpu.r[0], c->cpu.r[1], c->cpu.r[2], c->cpu.r[3]);
	// printf("+--RAM--------------------------+\n");
	// for (int i = 0; i < 8; ++i) {
	// 	printf("| \x1B[33m%02x\x1B[0m %02x | \x1B[33m%02x\x1B[0m %02x | \x1B[33m%02x\x1B[0m %02x | \x1B[33m%02x\x1B[0m %02x |\n", 4*i, c->ram[4*i], 4*i+1, c->ram[4*i+1], 4*i+2, c->ram[4*i+2], 4*i+3, c->ram[4*i+3] );
	// }
	printf("+--COMPUTER-STATE--+\n");
	printf("+--CPU---------------------------+\n");
	printf("| PC | C | N | R0 | R1 | R2 | R3 |\n");
	printf("| \x1B[33m%02x\x1B[0m | \x1B[33m%i\x1B[0m | \x1B[33m%i\x1B[0m | \x1B[33m%02x\x1B[0m | \x1B[33m%02x\x1B[0m | \x1B[33m%02x\x1B[0m | \x1B[33m%02x\x1B[0m |\n", c->cpu.pc, c->cpu.C != 0, c->cpu.N != 0, c->cpu.r[0], c->cpu.r[1], c->cpu.r[2], c->cpu.r[3]);
	printf("+--RAM--------------------------+\n");
	for (int i = 0; i < 8; ++i) {
		// printf("| %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m |\n", 4*i, c->ram[4*i], 4*i+1, c->ram[4*i+1], 4*i+2, c->ram[4*i+2], 4*i+3, c->ram[4*i+3] );
		printf("| %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m | %02x \x1B[33m%02x\x1B[0m |\n", i, c->ram[i], 8+i, c->ram[8+i], 16+i, c->ram[16+i], 24+i, c->ram[24+i] );
	}
}

Computer new_computer() {
	Computer c;
	c.power = true;
	c.cpu.pc = 0;
	c.cpu.C  = 0;
	c.cpu.N  = 0;
	c.cpu.r[0] = 0;
	c.cpu.r[1] = 0;
	c.cpu.r[2] = 0;
	c.cpu.r[3] = 0;

	for( int i = 0 ; i < RAM_SIZE ; i++ ) {
		c.ram[i]=0;
	}

	return c;
}

InstrCall decode_instruction( BYTE ir ) {
	BYTE istruction_code = ir>>5;

	InstrCall out;
	out.instr = ERR;
	out.arg1  = 0;
	out.arg2  = 0;

	// print_red("%i ",ir);

	switch( istruction_code ) {
		case 0:
			out.instr = LDM;
			out.arg1  = ir & 31 ;
			break;
		case 1:
			out.instr = LDI;
			out.arg1  = ir & 31 ;
			break;
		case 2:
			out.instr = STR;
			out.arg1  = ir & 31 ;
			break;
		case 3:
			if( ir & 16 ) {
				out.instr = CLR;
				out.arg1  = ir    & 3 ;
			} else {
				out.instr = ACC;
				out.arg1  = ir>>2 & 3 ;
				out.arg2  = ir    & 3 ;
			}
			break;
		case 4:
			out.instr = B;
			out.arg1  = ir & 31 ;
			break;
		case 5:
			out.instr = BC;
			out.arg1  = ir & 31 ;
			break;
		case 6:
			out.instr = BN;
			out.arg1  = ir & 31 ;
			break;
		case 7:
			if( ir & 16 ) {
				out.instr = END;
			} else {
				out.instr = CMP;
				out.arg1  = ir>>2 & 3 ;
				out.arg2  = ir    & 3 ;
			}
			break;
		default:
			ERROR("cannot decode this instruction : %i", ir);
	}
	return out;
}

void print_LDM( BYTE address )     { ;print_green( "LDM %i", address ); }
void print_LDI( BYTE value   )     { ;print_green( "LDI %i", value   ); }
void print_STR( BYTE address )     { ;print_green( "STR %i", address ); }
void print_ACC( BYTE r1, BYTE r2 ) { ;print_green( "ACC R%i R%i", r1, r2 ); }
void print_B  ( BYTE address )     { ;print_green( "B   %i", address ); }
void print_BC ( BYTE address )     { ;print_green( "BC  %i", address ); }
void print_BN ( BYTE address )     { ;print_green( "BN  %i", address ); }
void print_CLR( BYTE reg     )     { ;print_green( "CLR R%i", reg    ); }
void print_CMP( BYTE r1, BYTE r2 ) { ;print_green( "CMP R%i R%i", r1, r2 ); }
void print_END()                   { ;print_green( "END"); }

void execute_LDM( Computer* c, BYTE address )     { print_LDM( address ); c->cpu.r[0] = c->ram[address]; }
void execute_LDI( Computer* c, BYTE value   )     { print_LDI( value   ); c->cpu.r[0] = value; }
void execute_STR( Computer* c, BYTE address )     { print_STR( address ); c->ram[address] = c->cpu.r[0]; }
void execute_ACC( Computer* c, BYTE r1, BYTE r2 ) { print_ACC( r1, r2 ); c->cpu.r[r1] += c->cpu.r[r2]; if( c->cpu.r[r1] + c->cpu.r[r2] >= 1<<8 ) {c->cpu.C=1;} else {c->cpu.C=0;} }
void execute_B  ( Computer* c, BYTE address )     { print_B  ( address ); c->cpu.pc = address; }
void execute_BC ( Computer* c, BYTE address )     { print_BC ( address ); if( c->cpu.C ) { c->cpu.pc = address; } }
void execute_BN ( Computer* c, BYTE address )     { print_BN ( address ); if( c->cpu.N ) { c->cpu.pc = address; } }
void execute_CLR( Computer* c, BYTE reg     )     { print_CLR( reg    ); c->cpu.r[reg] = 0; }
void execute_CMP( Computer* c, BYTE r1, BYTE r2 ) { print_CMP( r1, r2 ); if( c->cpu.r[r1] - c->cpu.r[r2] < 0     ) { c->cpu.N = 1; } else { c->cpu.N = 0; }
												  if( c->cpu.r[r1] + c->cpu.r[r2] >= 1<<8 ) { c->cpu.C = 1; } else { c->cpu.C = 0; } }
void execute_END( Computer* c )                   { print_END(); c->power=false;}

void execute_instruction( Computer* c, InstrCall instr ) {
	switch( instr.instr ) {
		case LDM:execute_LDM( c, instr.arg1 );break;
		case LDI:execute_LDI( c, instr.arg1 );break;
		case STR:execute_STR( c, instr.arg1 );break;
		case ACC:execute_ACC( c, instr.arg1, instr.arg2 );break;
		case B  :execute_B  ( c, instr.arg1 );break;
		case BC :execute_BC ( c, instr.arg1 );break;
		case BN :execute_BN ( c, instr.arg1 );break;
		case CLR:execute_CLR( c, instr.arg1 );break;
		case MOV:ERROR("MOV not implemented"); break;
		case CMP:execute_CMP( c, instr.arg1, instr.arg2 );break;
		case END:execute_END( c );break;
		default: ERROR("this instruction does not exist");
	}
}

void print_instruction( InstrCall instr ) {
	switch( instr.instr ) {
		case LDM:print_LDM( instr.arg1 );break;
		case LDI:print_LDI( instr.arg1 );break;
		case STR:print_STR( instr.arg1 );break;
		case ACC:print_ACC( instr.arg1, instr.arg2 );break;
		case B  :print_B  ( instr.arg1 );break;
		case BC :print_BC ( instr.arg1 );break;
		case BN :print_BN ( instr.arg1 );break;
		case CLR:print_CLR( instr.arg1 );break;
		case MOV:ERROR("MOV not implemented"); break;
		case CMP:print_CMP( instr.arg1, instr.arg2 );break;
		case END:print_END();break;
		default: ERROR("this instruction does not exist");
	}
}


void load_program( Computer* c, BYTE* program, int length ) {
	load_to_ram( c, program, length, 0 );
}

void load_to_ram( Computer* c, BYTE* data, int length, int position ) {
	if( length + position == RAM_SIZE ) {
		ERROR("MEMORY OVERFLOW");
	}
	for( BYTE i = position ; i < length ; i++ ) {
		c->ram[i % RAM_SIZE] = data[i % RAM_SIZE];
	}
}

void computer_one_cycle( Computer* computer ) {
	// lire instruction à executer
	BYTE ir = computer->ram[computer->cpu.pc];
	computer->cpu.pc = (computer->cpu.pc + 1)%RAM_SIZE;

	// décoder instruction
	InstrCall instr = decode_instruction( ir );

	// exécuter instruction
	execute_instruction( computer, instr );
	// computer_print( computer );
}

void computer_run( Computer* computer ) {
	computer->power = true;
	while( computer->power ) {
		computer_one_cycle( computer );
	}
}

void computer_run_interactive( Computer* computer ) {
	computer->power = true;

	const char* HELP = "type :\n  [c] to coninue\n  [p] to print and continue\n  [q] to shutdown\n  [h] to print this message again\n";
	printf( HELP );

	while( computer->power ) {

		bool cont = false;
		while( !cont ) {
			char typed = getchar();
			if ( typed == 'p') {
				computer_print( computer );
				cont=true;
			}
			if ( typed == 'c') {
				cont=true;
			}
			if ( typed == 'q') {
				computer->power = false;
				cont=true;
			}
			if ( typed == 'h') {
				printf( HELP );
			}
		}

		computer_one_cycle( computer );
		if( ! computer->power ) {
			computer_print( computer );
		}
	}
}
