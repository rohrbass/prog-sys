#pragma once

#define RAM_SIZE 32

typedef unsigned char BYTE;

typedef enum bool {false, true} bool;

typedef struct {
	BYTE r[4];
	BYTE pc;
	bool C;
	bool N;
} CPU;

typedef BYTE RAM[RAM_SIZE];

typedef struct {
	CPU cpu;
	RAM ram;
	bool power;
} Computer;

void computer_print( Computer* c );
Computer new_computer();

void computer_run( Computer* computer );
void computer_run_interactive( Computer* computer );
void computer_run_verbose( Computer* computer );

// INSTRUCTION SET

typedef enum Instruction { LDM, LDI, STR, ACC, B, BC, BN, CLR, MOV, CMP, END ,ERR } Instruction;
typedef struct {
	Instruction instr;
	BYTE arg1;
	BYTE arg2;
} InstrCall;

InstrCall decode_instruction( BYTE ir );

void execute_instruction( Computer* c, InstrCall instr );

void print_instruction( InstrCall instr );

// NOTE THAT THE PROGRAM IS ALWAYS LOADED AT ADDRESS 0

void load_program( Computer* c, BYTE* program, int length );
void load_to_ram ( Computer* c, BYTE* data,    int length, int position );

