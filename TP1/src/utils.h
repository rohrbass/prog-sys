#pragma once

#include <stdio.h>

#define ERROR(...) term_color_red();printf(__VA_ARGS__);printf("\n");term_color_normal();
#define UNUSED(x) (void)(x)
#define min(x,y) (((x)<(y))?(x):(y))

void term_color_normal();
void term_color_red();
void term_color_green();
void term_color_yellow();
void term_color_blue();
void term_color_magenta();
void term_color_cyan();
void term_color_white();

#define print_green(...) term_color_green();printf(__VA_ARGS__);printf("\n");term_color_normal();
#define print_red(...) term_color_red();printf(__VA_ARGS__);printf("\n");term_color_normal();
