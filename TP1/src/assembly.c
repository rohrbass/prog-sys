#include "utils.h"
#include "assembly.h"
#include <string.h>
#include <stdlib.h>

BYTE ASM_to_BYTE_one_line( char* asm_code ) {

	char instruction[4];
	instruction[0]=asm_code[0];
	instruction[1]=asm_code[1];
	instruction[2]=asm_code[2];
	instruction[3]='\0';

	BYTE out;

	char arg[3];
	arg[0]=asm_code[4];
	arg[1]=asm_code[5];
	arg[2]='\0';


	char arg1[2];
	arg1[0] = asm_code[4];
	arg1[1] = '\0';

	char arg2[2];
	arg2[0] = asm_code[6];
	arg2[1] = '\0';

	if( strcmp(instruction,"LDM") == 0 ) { out = (0<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"LDI") == 0 ) { out = (1<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"STR") == 0 ) { out = (2<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"B  ") == 0 ) { out = (4<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"BC ") == 0 ) { out = (5<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"BN ") == 0 ) { out = (6<<5); out += (atoi(arg) % 32); }
	if( strcmp(instruction,"CLR") == 0 ) { out = (3<<5) + 16; out += (atoi(arg1) % 4); }
	if( strcmp(instruction,"END") == 0 ) { out = (7<<5) + 16; }

	if( strcmp(instruction,"ACC") == 0 ) { out = (3<<5); out += ( (atoi(arg1) % 4) << 2 ); out += (atoi(arg2) % 4); }
	if( strcmp(instruction,"CMP") == 0 ) { out = (7<<5); out += ( (atoi(arg1) % 4) << 2 ); out += (atoi(arg2) % 4); }

	// print_instruction(decode_instruction(0 + (0<<5)));
	// print_instruction(decode_instruction(1 + (0<<5)));
	// print_instruction(decode_instruction(2 + (0<<5)));
	// print_instruction(decode_instruction(3 + (0<<5)));
	// print_instruction(decode_instruction(4 + (0<<5)));

	// print_instruction(decode_instruction(0 + (1<<5) ));
	// print_instruction(decode_instruction(1 + (1<<5) ));
	// print_instruction(decode_instruction(2 + (1<<5) ));
	// print_instruction(decode_instruction(3 + (1<<5) ));
	// print_instruction(decode_instruction(4 + (1<<5) ));

	// print_instruction(decode_instruction(0 + (2<<5) ));
	// print_instruction(decode_instruction(1 + (2<<5) ));
	// print_instruction(decode_instruction(2 + (2<<5) ));
	// print_instruction(decode_instruction(3 + (2<<5) ));
	// print_instruction(decode_instruction(4 + (2<<5) ));

	// // ACC
	// print_instruction(decode_instruction(0 + (3<<5) ));
	// print_instruction(decode_instruction(1 + (3<<5) ));
	// print_instruction(decode_instruction(2 + (3<<5) ));
	// print_instruction(decode_instruction(3 + (3<<5) ));
	// print_instruction(decode_instruction(4 + (3<<5) ));

	// // CLR
	// print_instruction(decode_instruction(0 + 16 + (3<<5) ));
	// print_instruction(decode_instruction(1 + 16 + (3<<5) ));
	// print_instruction(decode_instruction(2 + 16 + (3<<5) ));
	// print_instruction(decode_instruction(3 + 16 + (3<<5) ));
	// print_instruction(decode_instruction(4 + 16 + (3<<5) ));

	// // B
	// print_instruction(decode_instruction(0 + (4<<5) ));
	// print_instruction(decode_instruction(1 + (4<<5) ));
	// print_instruction(decode_instruction(2 + (4<<5) ));
	// print_instruction(decode_instruction(3 + (4<<5) ));
	// print_instruction(decode_instruction(4 + (4<<5) ));

	// // BC
	// print_instruction(decode_instruction(0 + (5<<5) ));
	// print_instruction(decode_instruction(1 + (5<<5) ));
	// print_instruction(decode_instruction(2 + (5<<5) ));
	// print_instruction(decode_instruction(3 + (5<<5) ));
	// print_instruction(decode_instruction(4 + (5<<5) ));

	// // BN
	// print_instruction(decode_instruction(0 + (6<<5) ));
	// print_instruction(decode_instruction(1 + (6<<5) ));
	// print_instruction(decode_instruction(2 + (6<<5) ));
	// print_instruction(decode_instruction(3 + (6<<5) ));
	// print_instruction(decode_instruction(4 + (6<<5) ));

	// // CMP
	// print_instruction(decode_instruction(0 + (7<<5) ));
	// print_instruction(decode_instruction(1 + (7<<5) ));
	// print_instruction(decode_instruction(2 + (7<<5) ));
	// print_instruction(decode_instruction(3 + (7<<5) ));
	// print_instruction(decode_instruction(4 + (7<<5) ));

	return out;
}