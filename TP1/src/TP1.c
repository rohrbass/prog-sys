#include "utils.h"
#include "assembly.h"
#include "computer.h"
#include <stdio.h>
#include <stdlib.h>

/*=====================================*/

int main( int argc, char *argv[] ) {

	if( argc < 1 ) {
		printf("USAGE : ./%s path/to/code.asm\n", argv[0] );
		exit(EXIT_FAILURE);
	}

	// printf("\n");
	// printf("         _|__|__|__|_       _____   \n");
	// printf("      __|            |_____|     |__\n");
	// printf("      __|            |_____|     |__\n");
	// printf("      __|            |_____|     |__\n");
	// printf("      __|            |_____|     |__\n");
	// printf("      __|            |_____|     |__\n");
	// printf("        |____________|     |_____|  \n");
	// printf("          |  |  |  |                \n");
	// printf("\n");

	Computer c = new_computer();;

	// computer_run(&c);

	// printf("%i\n", atoi("101 10"));

	// print_instruction(decode_instruction(ASM_to_BYTE_one_line("ACC 0 0")));
	// print_instruction(decode_instruction(ASM_to_BYTE_one_line("CLR 1")));


	BYTE program[RAM_SIZE];
	int program_len=0;


	FILE *fp = fopen(argv[1], "r");
	if( fp == NULL ) {
		ERROR("file \"%s\" does not exist",argv[1]);
	}

	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	while ((read = getline(&line, &len, fp)) != -1) {
		if( line[0]=='#' ) {
			continue;
		}
		if( line[0]=='\n' ) {
			continue;
		}
		program[program_len] = ASM_to_BYTE_one_line(line);
		program_len++;
	}

	fclose(fp);
	if( line ) {
		free(line);
	}

	load_program(&c, program, program_len);

	computer_run_interactive(&c);
	// computer_run_verbose(&c);
	// computer_print(&c);

	return 0;
}
