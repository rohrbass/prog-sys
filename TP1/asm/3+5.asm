# ram[20] = 3+5

### INSTRUCTION HINT ###
# 
# LDM addr  〉 R0=RAM[addr]
# LDI val   〉 R0=val
# STR addr  〉 RAM[addr]=R0
# 
# ACC R1 R2 〉 R1 = R1 + R2 (C=1 si débordement)
# 
# B   addr  〉 PC=addr
# BC  addr  〉 if(C) PC=addr
# BN  addr  〉 if(N) PC=addr
# 
# CLR R     〉 R=0
# 
# CMP R1 R1 〉 N=(R1-R2<0) C=(R1+R2>2^8)
# 


#### R1 = 5
# R1 = 0
CLR 1
# R0 = 3
LDI 5
# R1 = R1 + R0 = 0 + 5 = 5
ACC 1 0

#### R0 = 3
LDI 3

#### R0 = R0 + R1 = 3 + 5
ACC 0 1

STR 20

END
