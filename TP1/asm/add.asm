# R1 + R2 avec test de dépassement

### INSTRUCTION HINT ###
# 
# LDM addr  〉 R0=RAM[addr]
# LDI val   〉 R0=val
# STR addr  〉 RAM[addr]=R0
# 
# ACC R1 R2 〉 R1 = R1 + R2 (C=1 si débordement)
# 
# B   addr  〉 PC=addr
# BC  addr  〉 if(C) PC=addr
# BN  addr  〉 if(N) PC=addr
# 
# CLR R     〉 R=0
# 
# CMP R1 R1 〉 N=(R1-R2<0) C=(R1+R2>2^8)
# 

ACC 1 2
BC  failed

...


B   end_failed
failed :
...

end_failed :


END
