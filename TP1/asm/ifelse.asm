# if else then

### INSTRUCTION HINT ###
# 
# LDM addr  〉 R0=RAM[addr]
# LDI val   〉 R0=val
# STR addr  〉 RAM[addr]=R0
# 
# ACC R1 R2 〉 R1 = R1 + R2 (C=1 si débordement)
# 
# B   addr  〉 PC=addr
# BC  addr  〉 if(C) PC=addr
# BN  addr  〉 if(N) PC=addr
# 
# CLR R     〉 R=0
# 
# CMP R1 R1 〉 N=(R1-R2<0) C=(R1+R2>2^8)
# 

# set R1 to 1
CLR 1
LDI 1
ACC 1 0


# if (R1) then ... else ...

CLR 0
CMP 0 1

# BN  goto_here
BN  11

CLR 0
CLR 1
CLR 2
CLR 3

# B   goto_end
B   15

# goto_here

CLR 3
CLR 2
CLR 1
CLR 0

# goto_end


END
