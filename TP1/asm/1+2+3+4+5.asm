# ram[20] = 1+2+...+5

### INSTRUCTION HINT ###
# 
# LDM addr  〉 R0=RAM[addr]
# LDI val   〉 R0=val
# STR addr  〉 RAM[addr]=R0
# 
# ACC R1 R2 〉 R1 = R1 + R2 (C=1 si débordement)
# 
# B   addr  〉 PC=addr
# BC  addr  〉 if(C) PC=addr
# BN  addr  〉 if(N) PC=addr
# 
# CLR R     〉 R=0
# 
# CMP R1 R1 〉 N=(R1-R2<0) C=(R1+R2>2^8)
# 

# loop

# MAX → R3 = 6
CLR 3
LDI 6
ACC 3 0

# COUNTER → R2 = 0
CLR 2

# ACCUMULATOR → R1 = 0
CLR 1

# ONE → R0 = 1
LDI 1


# ACCUMULATOR += COUNTER
ACC 1 2
# COUNTER++
ACC 2 0

CMP 2 3
BN  6


# WRITE RESULT 
CLR 0
ACC 0 1
STR 20
# BC 5


END
